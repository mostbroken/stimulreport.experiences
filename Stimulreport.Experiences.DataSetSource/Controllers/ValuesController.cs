﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Stimulreport.Experiences.DataSetSource.BO;
using Stimulsoft.Report;

namespace Stimulreport.Experiences.DataSetSource.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class ValuesController : ControllerBase
    {
        const string Template = "Report.mrt";

        public ActionResult<string> Get()
        {
            //создание отчета
            StiReport report = new StiReport();

            //загрузка шаблона
            report.Load(Template);
            //report.Load(TemplateRel);

            //заполнение данными
            //FillData(report);
            FillDataRel(report);

            //применение данных к отчету
            report.Render();

            var stream = new MemoryStream();
            //экспорт отчета в поток
            report.ExportDocument(StiExportFormat.Pdf, stream);
            stream.Position = 0;

            return new FileStreamResult(stream, new MediaTypeHeaderValue("application/pdf"));
        }

        private void FillData(StiReport report)
        {
            var document = XDocument.Load(System.IO.File.OpenRead("data.xml"));
            var creator = new ReportDataSetCreator();
            report.RegBusinessObject("Root", creator.Create(document));
        }

        private void FillDataRel(StiReport report)
        {
            var document = XDocument.Load(System.IO.File.OpenRead("data.xml"));

            var creator = new ReportDataSetCreator();
            var reportDataSet = creator.Create(document).GetDataSet();

            report.RegData(reportDataSet);
        }
    }
}