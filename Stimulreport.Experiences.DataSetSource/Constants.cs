﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stimulreport.Experiences.DataSetSource
{
    public static class Constants
    {
        /// <summary>
        /// Тип документа. Для документа типа Смета должен быть именно такой.
        /// </summary>
        public const string DocumentType = "{2B0470FD-477C-4359-9F34-EEBE36B7D340}";
        public const string ServicesChapterCaption = "Услуги";
        public const string MaterialsChapterCaption = "Материалы";
        public const string MachineAndMechanismsChapterCaption = "Машины и механизмы";
        public const string PositionCommentDefaultValue = "";
        public const string PositionPriceLevelDefaultValue = "2001";
        public const string MaterialAndServiceCostItogDataType = "ForOneBase";
        public const string MechanismCostItogDataType = "TotalFo";
    }
}
