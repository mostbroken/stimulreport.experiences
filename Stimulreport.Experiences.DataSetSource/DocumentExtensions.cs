﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Stimulreport.Experiences.DataSetSource
{
    /// <summary>
    /// Предоставляет API, упрощающее обработку xml, полученного из ГрандСметы.
    /// </summary>
    public static class DocumentExtensions
    {
        private const string ChapterCaptionAttributeName = "Caption";
        private const string ChapterElementName = "Chapter";
        private const string ChaptersElementName = "Chapters";
        private const string PositionElementName = "Position";
        private const string PositionNumberAttributeName = "Number";
        private const string QuantityElementName = "Quantity";
        private const string ResultAttributeName = "Result";
        private const string ItogElementName = "Itog";
        private const string ItogResElementName = "ItogRes";
        private const string ItogBimElementName = "ItogBim";
        private const string UserDataElementName = "UserData";
        private const string UnitsAttributeName = "Units";
        private const string PositionCaptionAttributeName = "Caption";
        private const string PZAttributeName = "PZ";
        private const string MTAttributeName = "MT";
        private const string PositionResourceIdAttributeName = "Data_1";
        private const string PositionWorkAssignmentCodeAttributeName = "Data_0";
        private const string PositionResourceCodeAttributeName = "Code";
        private const string PrintedItemsElementName = "Toir_Printed";

        private static readonly XName IdAttributeName = XName.Get("Toir_ID");

        #region document api

        public static IEnumerable<XElement> GetChapters(this XDocument xDocument)
        {
            return xDocument.Root.Elements(ChaptersElementName).Elements(ChapterElementName);
        }

        public static string TryGetComment(this XDocument xDocument)
        {
            return xDocument.Root.Attribute("Comment")?.Value;
        }

        public static XElement GetDocumentItog(this XDocument xDocument)
        {
            return xDocument.Root.Element(ItogElementName);
        }

        public static XElement GetDocumentItog(this XDocument xDocument, CalculationType calculationType)
        {
            switch (calculationType)
            {
                case CalculationType.BasicIndex:
                    return xDocument.Root.Element(ItogElementName)?.Element(ItogBimElementName);
                case CalculationType.Resource:
                    return xDocument.Root.Element(ItogElementName)?.Element(ItogResElementName);
                default:
                    throw new ArgumentException(nameof(calculationType));
            }
        }

        public static async Task<XDocument> LoadAsync(this Stream stream)
        {
            return await XDocument.LoadAsync(stream, LoadOptions.SetLineInfo, CancellationToken.None);
        }

        public static XDocument Load(this Stream stream)
        {
            return XDocument.Load(stream, LoadOptions.SetLineInfo);
        }

        public static MemoryStream GetStream(this XDocument xDocument)
        {
            var xmlStream = new MemoryStream();
            xDocument.Save(xmlStream);
            xmlStream.Position = 0;
            return xmlStream;
        }

        public static void AddPrintedItems(this XDocument xDocument, IEnumerable<string> itemsIds)
        {
            var printedItemsElement = xDocument.Root.Element(PrintedItemsElementName);
            if (printedItemsElement != null)
                printedItemsElement.Remove();

            printedItemsElement = new XElement(PrintedItemsElementName, itemsIds.Select(item => new XElement(XName.Get("Item"), item)));
            xDocument.Root.Add(printedItemsElement);
        }

        public static IEnumerable<string> GetPrintedItems(this XDocument xDocument)
        {
            var printedItemsElement = xDocument.Root.Element(PrintedItemsElementName);
            if (printedItemsElement == null)
                return new List<string>();

            return printedItemsElement.Elements().Select(e => e.Value);
        }

        #endregion

        #region chapter api

        public static IEnumerable<XElement> GetPositions(this XElement chapterElement)
        {
            CheckIsChapter(chapterElement);

            return chapterElement.Elements(PositionElementName);
        }

        public static string TryGetChapterSysId(this XElement chapterElement)
        {
            CheckIsChapter(chapterElement);
            return chapterElement.Attribute("SysID")?.Value;
        }

        public static string GetChapterSysId(this XElement chapterElement)
        {
            return chapterElement.TryGetChapterSysId() ?? throw new ArgumentException("В разделе не указано значение SysIDs");
        }

        public static bool IsService(this XElement chapter)
        {
            CheckIsChapter(chapter);

            var caption = chapter.Attribute(ChapterCaptionAttributeName)?.Value;
            return caption != Constants.MachineAndMechanismsChapterCaption && caption != Constants.MaterialsChapterCaption;
        }

        public static bool IsMechanism(this XElement chapter)
        {
            CheckIsChapter(chapter);

            var caption = chapter.Attribute(ChapterCaptionAttributeName)?.Value;
            return caption == Constants.MachineAndMechanismsChapterCaption;
        }

        public static bool IsMaterial(this XElement chapter)
        {
            CheckIsChapter(chapter);

            var caption = chapter.Attribute(ChapterCaptionAttributeName)?.Value;
            return caption == Constants.MaterialsChapterCaption;
        }

        public static XElement GetChapterItog(this XElement chapter)
        {
            CheckIsChapter(chapter);
            return chapter.Element(ItogElementName);
        }
        public static XElement GetChapterItog(this XElement chapter, CalculationType calculationType)
        {
            CheckIsChapter(chapter);

            switch (calculationType)
            {
                case CalculationType.BasicIndex:
                    return chapter.Element(ItogElementName)?.Element(ItogBimElementName);
                case CalculationType.Resource:
                    return chapter.Element(ItogElementName)?.Element(ItogResElementName);
                default:
                    throw new ArgumentException(nameof(calculationType));
            }
        }

        public static void CheckIsChapter(this XElement chapter)
        {
            if (chapter.Name != ChapterElementName)
                throw new ArgumentException($"Ожидается элемент с именем \"{ChapterElementName}\"");
        }

        #endregion

        #region position api

        public static bool TryGetPositionNumber(this XElement positionElement, out int result)
        {
            result = default;
            CheckIsPosition(positionElement);

            var numberAttr = positionElement.Attribute(PositionNumberAttributeName);
            if (numberAttr == null || string.IsNullOrWhiteSpace(numberAttr.Value))
                return false;

            return int.TryParse(numberAttr.Value, out result);
        }

        public static int GetPositionNumber(this XElement positionElement)
        {
            if (!positionElement.TryGetPositionNumber(out var result))
                throw new ArgumentException("В позиции отсутствует номер или значение имеет неверный формат");

            return result;
        }

        public static bool PositionHasQuantity(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(QuantityElementName)?.Attribute(ResultAttributeName) != null;
        }

        public static bool TryGetPositionQuantity(this XElement positionElement, out decimal result)
        {
            result = default;
            CheckIsPosition(positionElement);
            var rawValue = positionElement.GetQuantityResultAttributeValue();

            if (!string.IsNullOrWhiteSpace(rawValue) && decimal.TryParse(rawValue, out result))
            {
                return true;
            }

            return false;
        }

        public static bool TryGetPositionQuantity(this XElement positionElement, out double result)
        {
            result = default;
            CheckIsPosition(positionElement);
            var rawValue = positionElement.GetQuantityResultAttributeValue();

            if (!string.IsNullOrWhiteSpace(rawValue) && double.TryParse(rawValue, out result))
            {
                return true;
            }

            return false;
        }

        public static void CheckIsPosition(this XElement positionElement)
        {
            if (positionElement.Name != PositionElementName)
                throw new ArgumentException($"Ожидается элемент с именем \"{PositionElementName}\"");
        }

        public static XElement TryGetPositionItogRes(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(ItogElementName)?.Element(ItogResElementName);
        }

        public static XElement TryGetPositionItogBim(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(ItogElementName)?.Element(ItogBimElementName);
        }

        public static string TryGetPositionResourceId(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(UserDataElementName)?.Attribute(PositionResourceIdAttributeName)?.Value;
        }

        public static string GetPositionResourceId(this XElement positionElement)
        {
            return positionElement.TryGetPositionResourceId() ?? throw new ArgumentException("В позиции отсутствует идентификатор ресурса, или значение имеет неверный формат");
        }

        public static string TryGetPositionResourceCode(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Attribute(PositionResourceCodeAttributeName)?.Value;
        }

        public static string GetPositionResourceCode(this XElement positionElement)
        {
            return positionElement.TryGetPositionResourceCode() ?? throw new ArgumentException("В позиции отсутствует код, или значение имеет неверный формат");
        }

        public static string GetPositionUnit(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Attribute(UnitsAttributeName)?.Value ?? throw new ArgumentException("В позиции не указана единица измерения");
        }

        public static string TryGetPositionCaption(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Attribute(PositionCaptionAttributeName)?.Value;
        }

        public static XElement GetPositionItog(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(ItogElementName);
        }

        public static XElement GetPositionItog(this XElement positionElement, CalculationType calculationType)
        {
            CheckIsPosition(positionElement);

            switch(calculationType)
            {
                case CalculationType.BasicIndex:
                    return positionElement.Element(ItogElementName)?.Element(ItogBimElementName);
                case CalculationType.Resource:
                    return positionElement.Element(ItogElementName)?.Element(ItogResElementName);
                default:
                    throw new ArgumentException(nameof(calculationType));
            }
        }

        public static string TryGetPositionWorkAssignmentCode(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(UserDataElementName)?.Attribute(PositionWorkAssignmentCodeAttributeName)?.Value;
        }

        public static string GetPositionWorkAssignmentCode(this XElement positionElement)
        {
            return positionElement.TryGetPositionWorkAssignmentCode() ?? throw new ArgumentException("В позиции отсутствует код, или значение имеет неверный формат");
        }

        #endregion

        #region itog API
        public static string TryGetItogCaption(this XElement itogElement)
        {
            CheckIsItog(itogElement);
            return itogElement.Attribute(PositionCaptionAttributeName)?.Value;
        }

        public static bool TryGetItogPZ(this XElement itogElement, out decimal result)
        {
            result = default;
            CheckIsItog(itogElement);
            var rawValue = itogElement.Attribute(PZAttributeName)?.Value;
            return !string.IsNullOrWhiteSpace(rawValue) && decimal.TryParse(rawValue, out result);
        }

        public static bool TryGetItogMT(this XElement itogElement, out decimal result)
        {
            result = default;
            CheckIsItog(itogElement);
            var rawValue = itogElement.Attribute(MTAttributeName)?.Value;
            return !string.IsNullOrWhiteSpace(rawValue) && decimal.TryParse(rawValue, out result);
        }

        public static IEnumerable<XElement> GetItogElements(this XElement itogElement)
        {
            CheckIsItog(itogElement);
            return itogElement.Elements(ItogElementName);
        }

        private static void CheckIsItog(this XElement itogElement)
        {
            if (itogElement.Name != ItogElementName)
                throw new ArgumentException($"Ожидается элемент с именем \"{ItogElementName}\"");
        }
        #endregion

        public static void SetId(this XElement element)
        {
            element.SetAttributeValue(IdAttributeName, Guid.NewGuid().ToString());
        }

        public static string GetId(this XElement itogItem)
        {
            return itogItem.Attribute(IdAttributeName)?.Value;
        }

        public static int GetLineNumber(this XElement xElement)
        {
            return ((IXmlLineInfo)xElement).LineNumber;
        }

        private static string GetQuantityResultAttributeValue(this XElement positionElement)
        {
            CheckIsPosition(positionElement);
            return positionElement.Element(QuantityElementName)?.Attribute(ResultAttributeName)?.Value;
        }

        #region report


        public static string TryGetChapterCaption(this XElement chapter)
        {
            CheckIsChapter(chapter);

            return chapter.Attribute(ChapterCaptionAttributeName)?.Value;
        }

        #endregion
    }
}
