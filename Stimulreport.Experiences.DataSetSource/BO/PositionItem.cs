﻿using System.Collections.Generic;

namespace Stimulreport.Experiences.DataSetSource.BO
{
    public class PositionItem
    {
        public string Id { get; set; }

        public string ChapterId { get; set; }

        public int EstimateNumber { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Unit { get; set; }

        public double? Count { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? TotalCost
        {
            get { return UnitCost * (decimal?)Count; }
        }
    }
}