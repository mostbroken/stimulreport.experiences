﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Stimulreport.Experiences.DataSetSource
{
    public class BasicIndexCalculationProcessor : ICalculationProcessor
    {
        public decimal? TryGetServiceCost(XElement position)
        {
            position.CheckIsPosition();
            var rawValue = GetPrizeAttributeValue(position, Constants.MaterialAndServiceCostItogDataType);
            return decimal.TryParse(rawValue, out var cost) ? (decimal?)cost : null;
        }

        public decimal? TryGetMaterialCost(XElement position)
        {
            position.CheckIsPosition();
            var rawValue = GetPrizeAttributeValue(position, Constants.MaterialAndServiceCostItogDataType);
            return decimal.TryParse(rawValue, out var cost) ? (decimal?)cost : null;
        }

        public decimal? TryGetMechanismCost(XElement position)
        {
            position.CheckIsPosition();
            var rawValue = GetPrizeAttributeValue(position, Constants.MechanismCostItogDataType);
            return decimal.TryParse(rawValue, out var cost) ? (decimal?)cost : null;
        }

        private string GetPrizeAttributeValue(XElement xElement, string dataType)
        {
            return xElement
                .Element("Itog")?
                .Element("ItogBim")?
                .Elements(XName.Get("Itog"))
                .FirstOrDefault(e => e.Attribute("DataType")?.Value == dataType)?
                .Attribute("PZ")?.Value;
        }
    }
}
