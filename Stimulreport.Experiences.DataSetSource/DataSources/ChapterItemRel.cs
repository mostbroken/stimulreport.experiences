﻿using System.Collections.Generic;

namespace Stimulreport.Experiences.DataSetSource.DataSources
{
    public class ChapterItemRel
    {
        public string Id { get; set; }

        public string SysId { get; set; }

        public string Description { get; set; }
    }
}