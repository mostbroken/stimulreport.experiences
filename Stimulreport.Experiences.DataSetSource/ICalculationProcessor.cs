﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Stimulreport.Experiences.DataSetSource
{
    /// <summary>
    /// Выполняет обработка Xml в зависимости от выбранного вида расчета: базисно-индексного или ресурсного
    /// </summary>
    public interface ICalculationProcessor
    {
        decimal? TryGetServiceCost(XElement position);

        decimal? TryGetMaterialCost(XElement position);

        decimal? TryGetMechanismCost(XElement position);
    }
}
