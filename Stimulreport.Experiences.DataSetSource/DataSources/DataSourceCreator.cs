﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Stimulreport.Experiences.DataSetSource.DataSources
{
    public class DataSourceCreator
    {
        public DataSourceResult Create(XDocument document)
        {
            var chapterItems = new List<ChapterItemRel>();
            var positionItems = new List<PositionItemRel>();

            var calculationType = CalculationType.BasicIndex;
            ICalculationProcessor calculationProcessor = new BasicIndexCalculationProcessor();

            foreach (var chapter in document.GetChapters())
            {
                var chapterItem = new ChapterItemRel
                {
                    Id = Guid.NewGuid().ToString(),
                    SysId = chapter.TryGetChapterSysId(),
                    Description = chapter.TryGetChapterCaption()
                };


                foreach (var position in chapter.GetPositions())
                {
                    var positionItem = new PositionItemRel
                    {
                        Id = Guid.NewGuid().ToString(),
                        ChapterId = chapterItem.Id,
                        EstimateNumber = position.GetPositionNumber(),
                        Name = position.TryGetPositionCaption(),
                        Code = position.TryGetPositionResourceCode(),
                        Count = position.TryGetPositionQuantity(out double count) ? (double?)count : null,
                        Unit = position.GetPositionUnit(),
                        UnitCost = chapter.IsService() ? calculationProcessor.TryGetServiceCost(position) : (chapter.IsMechanism() ? calculationProcessor.TryGetMechanismCost(position) : calculationProcessor.TryGetMechanismCost(position)),
                        //SummaryItems = GetSummaryHierarchy(pos.GetPositionItog(calculationType), null).ToList()
                    };

                    positionItems.Add(positionItem);
                }
                //chapterItem.PositionItems = chapter.GetPositions().Select(pos => new PositionItem
                //{
                //    EstimateNumber = pos.GetPositionNumber(),
                //    Name = pos.TryGetPositionCaption(),
                //    Code = pos.TryGetPositionResourceCode(),
                //    Count = pos.TryGetPositionQuantity(out double count) ? (double?)count : null,
                //    Unit = pos.GetPositionUnit(),
                //    UnitCost = chapter.IsService() ? calculationProcessor.TryGetServiceCost(pos) : (chapter.IsMechanism() ? calculationProcessor.TryGetMechanismCost(pos) : calculationProcessor.TryGetMechanismCost(pos)),
                //    SummaryItems = GetSummaryHierarchy(pos.GetPositionItog(calculationType), null).ToList()
                //}).ToList();

                //chapterItem.SummaryItems = GetSummaryHierarchy(chapter.GetChapterItog(calculationType), null).ToList();
                chapterItems.Add(chapterItem);
            }

            //var rootReportObject = new RootReportObject
            //{
            //    ChapterItems = chapterItems,
            //    SummaryItems = GetSummaryHierarchy(document.GetDocumentItog(calculationType), null).ToList()
            //};



            var res = new DataSourceResult()
            {
                ChapterItems = chapterItems,
                PositionItems = positionItems
            };

            return res;
        }
    }

    public class DataSourceResult
    {
        public List<ChapterItemRel> ChapterItems { get; set; }

        public List<PositionItemRel> PositionItems { get; set; }

        public List<SummaryItemRel> SummaryItems { get; set; }
    }
}
