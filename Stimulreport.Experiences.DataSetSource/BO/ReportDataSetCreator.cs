﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Stimulreport.Experiences.DataSetSource.BO
{
    public class ReportDataSetCreator
    {
        public ReportDataSet Create(XDocument document)
        {
            var chapterItems = new List<ChapterItem>();
            var positionItems = new List<PositionItem>();
            var chapterSummaryItems = new List<SummaryItem>();
            var positionSummaryItems = new List<SummaryItem>();
            var estimateSummaryItems = new List<SummaryItem>();

            var calculationType = CalculationType.BasicIndex;
            ICalculationProcessor calculationProcessor = new BasicIndexCalculationProcessor();

            foreach (var chapter in document.GetChapters())
            {
                var chapterItem = new ChapterItem(Guid.NewGuid().ToString(), chapter.TryGetChapterSysId(), chapter.TryGetChapterCaption());

                foreach (var pos in chapter.GetPositions())
                {
                    var positionItem = new PositionItem
                    {
                        Id = Guid.NewGuid().ToString(),
                        ChapterId = chapterItem.Id,
                        EstimateNumber = pos.GetPositionNumber(),
                        Name = pos.TryGetPositionCaption(),
                        Code = pos.TryGetPositionResourceCode(),
                        Count = pos.TryGetPositionQuantity(out double count) ? (double?)count : null,
                        Unit = pos.GetPositionUnit(),
                        UnitCost = chapter.IsService() ? calculationProcessor.TryGetServiceCost(pos) : (chapter.IsMechanism() ? calculationProcessor.TryGetMechanismCost(pos) : calculationProcessor.TryGetMechanismCost(pos))
                    };

                    positionItems.Add(positionItem);
                    positionSummaryItems.AddRange(GetSummaryHierarchy(pos.GetPositionItog(calculationType), null).Select(si => si.UpdatePosition(positionItem.Id)));
                }

                chapterItems.Add(chapterItem);
                chapterSummaryItems.AddRange(GetSummaryHierarchy(chapter.GetChapterItog(calculationType), null).Select(si => si.UpdateChapter(chapterItem.Id)));
            }

            var rootReportObject = new ReportDataSet
            {
                ChapterItems = chapterItems,
                ChapterSummaryItems = chapterSummaryItems,
                PositionItems = positionItems,
                PositionSummaryItems = positionSummaryItems,
                EstimateSummaryItems = GetSummaryHierarchy(document.GetDocumentItog(calculationType), null).ToList()
            };

            return rootReportObject;
        }

        private SummaryItem GetItogItem(XElement itog, string id, string parentId)
        {
            return new SummaryItem
            {
                Id = id,
                ParentId = parentId,
                Description = itog.TryGetItogCaption(),
                Amount = itog.TryGetItogPZ(out var amount) ? (decimal?)amount : null
            };
        }

        IEnumerable<SummaryItem> GetSummaryHierarchy(XElement root, string rootId)
        {
            foreach (var child in root.Elements())
            {
                var childId = Guid.NewGuid().ToString();

                yield return GetItogItem(child, childId, rootId);

                foreach (var grandChild in GetSummaryHierarchy(child, childId))
                {
                    yield return grandChild;
                }
            }
        }
    }
}