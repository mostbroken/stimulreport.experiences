﻿namespace Stimulreport.Experiences.DataSetSource.DataSources
{
    public class SummaryItemRel
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string Description { get; set; }

        public decimal? Amount { get; set; }

        public string ChapterId { get; set; }

        public string PositionId { get; set; }
    }
}