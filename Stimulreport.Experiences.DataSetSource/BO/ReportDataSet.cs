﻿using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Collections;
using System;
using System.Reflection;

namespace Stimulreport.Experiences.DataSetSource.BO
{
    public class ReportDataSet
    {
        public List<ChapterItem> ChapterItems { get; set; }

        public List<PositionItem> PositionItems { get; set; }

        public List<SummaryItem> EstimateSummaryItems { get; set; }

        public List<SummaryItem> ChapterSummaryItems { get; internal set; }

        public List<SummaryItem> PositionSummaryItems { get; internal set; }

        public DataSet GetDataSet()
        {
            var dataSet = new DataSet(nameof(ReportDataSet));

            var listProperties = this.GetType()
                .GetProperties()
                .Where(p => p.PropertyType.IsGenericType && typeof(List<>).IsAssignableFrom(p.PropertyType.GetGenericTypeDefinition()));

            foreach (var listProperty in listProperties)
            {
                var itemType = listProperty.PropertyType.GenericTypeArguments[0];

                var table = new DataTable(listProperty.Name);
                var columns = itemType.GetProperties().Select(p => new DataColumn(p.Name, IsNullable(p) ? GetNullableType(p) : p.PropertyType) { AllowDBNull = IsNullable(p) || !p.PropertyType.IsValueType}).ToArray();
                table.Columns.AddRange(columns);
                var items = (IEnumerable)listProperty.GetValue(this);

                foreach (var item in items)
                {
                    var values = columns.Select(c => itemType.GetProperty(c.ColumnName).GetValue(item)).ToArray();
                    table.Rows.Add(values);
                }

                dataSet.Tables.Add(table);
            }

            return dataSet;
        }

        private bool IsNullable(PropertyInfo propertyInfo )
        {
            return propertyInfo.PropertyType.IsGenericType && typeof(Nullable<>).IsAssignableFrom(propertyInfo.PropertyType.GetGenericTypeDefinition());
        }

        private Type GetNullableType(PropertyInfo propertyInfo)
        {
            return Nullable.GetUnderlyingType(propertyInfo.PropertyType);
        }
    }
}