﻿namespace Stimulreport.Experiences.DataSetSource.BO
{
    public class SummaryItem
    {
        public string Id { get; set; }

        public string ChapterId { get; set; }

        public string PositionId { get; set; }

        public string ParentId { get; set; }

        public string Description { get; set; }

        public decimal? Amount { get; set; }

        public SummaryItem UpdateChapter(string chapterId)
        {
            ChapterId = chapterId;
            return this;
        }

        public SummaryItem UpdatePosition(string positionId)
        {
            PositionId = positionId;
            return this;
        }
    }
}