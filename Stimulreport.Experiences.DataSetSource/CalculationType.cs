﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stimulreport.Experiences.DataSetSource
{
    /// <summary>
    /// Вид расчета.
    /// </summary>
    public enum CalculationType
    {
        /// <summary>
        /// Базисно-индексный.
        /// </summary>
        BasicIndex,

        /// <summary>
        /// Ресурсный.
        /// </summary>
        Resource
    }
}
