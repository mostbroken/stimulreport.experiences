﻿using System.Collections.Generic;

namespace Stimulreport.Experiences.DataSetSource.BO
{
    public class ChapterItem
    {
        public ChapterItem(string id, string sysId, string description)
        {
            Id = id;
            SysId = sysId;
            Description = description;
        }

        public string Id { get; set; }

        public string SysId { get; set; }

        public string Description { get; set; }
    }
}